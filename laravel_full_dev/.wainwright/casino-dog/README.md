## Installation

You can install the package via composer:
```bash
composer require wainwright/casino-dog
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --tag="casino-dog-migrations"
php artisan migrate
```

You can publish the config file with:

```bash
php artisan vendor:publish --tag="casino-dog-config"
```

This is the contents of the published config file:

```php
return [
];
```

Optionally, you can publish the views using

```bash
php artisan vendor:publish --tag="casino-dog-views"
```

